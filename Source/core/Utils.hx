package core;

typedef Point = {
    var x: Int;
    var y: Int;
}

typedef TileBitMask = {
    var corners: Int;
    var cardinal: Int;
    var mask: Int;
}

typedef Tile = {
    > Point,
    var type: Int;
    var ?bitmask: TileBitMask;
    var ?mask: Int;
    var ?tilesetNo: Int;
}

typedef TilesArray = Array<Tile>;
typedef TilesMap = Map<String, Int>;  // "x,y" => TilesArray.index

typedef Level = {
    var tilesA: TilesArray;
    var tilesM: TilesMap;
    var width: Int;
    var height: Int;
}

// 8-bit tile mask
// NW          N -north      NE
//         *****************
//         *   8 * 16 *  1 *
// W -west * 128 *  X * 32 * E -east
//         *   4 * 64 *  2 *
//         *****************
// SW          S -south      SE
//
//

class Utils {
    public static function getTilesetNo(tile:Tile):Int {
        var bitmask = tile.bitmask;
        var k = 0;

        if(bitmask.cardinal == 0) {
            k = bitmask.corners;  // 0 - 15
        }

        if(bitmask.cardinal == 16) {
            if([0, 1, 8, 9].indexOf(bitmask.corners) != -1) {
                k = 16;  // 28
            }
            if([2, 3, 10, 11].indexOf(bitmask.corners) != -1) {
                k = 17;  // 27
            }
            if([4, 5, 12, 13].indexOf(bitmask.corners) != -1) {
                k = 18;  // 26
            }
            if([6, 7, 14, 15].indexOf(bitmask.corners) != -1) {
                k = 19;  // 23
            }
        }

        if(bitmask.cardinal == 32) {
            if([0, 1, 2, 3].indexOf(bitmask.corners) != -1) {
                k = 20;  // 21
            }
            if([4, 5, 6, 7].indexOf(bitmask.corners) != -1) {
                k = 21;  // 24
            }
            if([8, 9, 10, 11].indexOf(bitmask.corners) != -1) {
                k = 22;  // 16
            }
            if([12, 13, 14, 15].indexOf(bitmask.corners) != -1) {
                k = 23;  // 30
            }
        }

        if(bitmask.cardinal == 16 + 32) {
            if([0, 1, 2, 3, 8, 9, 10, 11].indexOf(bitmask.corners) != -1) {
                // if(tile.type == 0) { 
                    k = 42;  // 8
                // } else {
                    // k = 42;  // 8
                // }
            }
            if([4, 5, 6, 7, 12, 13, 14, 15].indexOf(bitmask.corners) != -1) {
                k = 25;  // 3
                // k = 43;  // 10
            }
        }

        if(bitmask.cardinal == 64) {
            if([0, 2, 4, 6].indexOf(bitmask.corners) != -1) {
                k = 26;  // 29
            }
            if([1, 3, 5, 7].indexOf(bitmask.corners) != -1) {
                k = 27;  // 19
            }
            if([8, 10, 12, 14].indexOf(bitmask.corners) != -1) {
                k = 28;  // 18
            }
            if([9, 11, 13, 15].indexOf(bitmask.corners) != -1) {
                k = 29;  // 22
            }
        }

        if(bitmask.cardinal == 16 + 64) {
            if(bitmask.corners >= 0 && bitmask.corners <= 15) {
                k = 40;  // 6
            }
        }

        if(bitmask.cardinal == 32 + 64) {
            if(bitmask.corners >= 0 && bitmask.corners <= 7) {
               k = 38;  // 0
            }
            if(bitmask.corners >= 8 && bitmask.corners <= 15) {
                k = 32;  // 11
            }
        }

        if(bitmask.cardinal == 16 + 32 + 64) {
            if(bitmask.corners >= 0 && bitmask.corners <= 15) {
                k = 33;  // 13
            }
        }

        if(bitmask.cardinal == 128) {
            if([0, 4, 8, 12].indexOf(bitmask.corners) != -1) {
                k = 34;  // 20
            }
            if([1, 5, 9, 13].indexOf(bitmask.corners) != -1) {
                k = 35;  // 17
            }
            if([2, 6, 10, 14].indexOf(bitmask.corners) != -1) {
                k = 36;  // 25
            }
            if([3, 7, 11, 15].indexOf(bitmask.corners) != -1) {
                k = 37;  // 31
            }
        }

        if(bitmask.cardinal == 128 + 16) {
            if([0, 1, 4, 5, 8, 9, 12, 13].indexOf(bitmask.corners) != -1) {
                k = 31;  // 9
            }
            if([2, 3, 6, 7, 10, 11, 14, 15].indexOf(bitmask.corners) != -1) {
                k = 39;  // 2
            }
        }

        if(bitmask.cardinal == 128 + 32) {
            if(bitmask.corners >= 0 && bitmask.corners <= 15) {
                k = 30;  // 7
            }
        }

        if(bitmask.cardinal == 128 + 32 + 16) {
            if(bitmask.corners >= 0 && bitmask.corners <= 15) {
                k = 41;  // 5
            }
        }

        if(bitmask.cardinal == 128 + 64) {
            if(bitmask.corners % 2 == 0) {
                k = 24;  // 1
            }
            if(bitmask.corners % 2 != 0) {
                k = 43;  // 10
            }
        }

        if(bitmask.cardinal == 128 + 64 + 16) {
            if(bitmask.corners >= 0 && bitmask.corners <= 15) {
                k = 44;  // 12
            }
        }

        if(bitmask.cardinal == 128 + 64 + 32) {
            if(bitmask.corners >= 0 && bitmask.corners <= 15) {
                k = 45;  // 4
            }
        }

        if(bitmask.cardinal == 128 + 64 + 32 + 16) {
            if(bitmask.corners >= 0 && bitmask.corners <= 15) {
                k = 46;  // 46
            }
        }

        var mask2tileset:Map<Int, Int> = [
            0 => 47, 1 => 44, 2 => 36, 3 => 43, 4 => 37, 5 => 14, 6 => 35, 7 => 32,
            8 => 45, 9 => 34, 10 => 15, 11 => 40, 12 => 42, 13 => 41, 14 => 33, 15 => 38,

            16 => 28, 17 => 27, 18 => 26, 19 => 23,

            20 => 21, 21 => 24, 22 => 16, 23 => 30,

            24 => 1, 25 => 3,

            26 => 29, 27 => 19, 28 => 18, 29 => 22,

            30 => 7,

            31 => 9, 32 => 11,

            33 => 13,

            34 => 20, 35 => 17, 36 => 25, 37 => 31,

            38 => 0, 39 => 2,

            40 => 6,

            41 => 5,

            42 => 8, 43 => 10,

            44 => 12,

            45 => 4,

            46 => 46,
            
            47 => 47
        ];

        return mask2tileset[k];
    }

    public static function calcBitMask(n:Tile, ne:Tile, e:Tile, se:Tile, s:Tile, sw:Tile, w:Tile, nw:Tile):TileBitMask {
        var n_v:Int = (n != null && n.type != 0) ? 1 : 0;
        var ne_v:Int = (ne != null && ne.type != 0) ? 1 : 0;
        var w_v:Int = (w != null && w.type != 0) ? 1 : 0;
        var e_v:Int = (e != null && e.type != 0) ? 1 : 0;
        var sw_v:Int = (sw != null && sw.type != 0) ? 1 : 0;
        var s_v:Int = (s != null && s.type != 0) ? 1 : 0;
        var se_v:Int = (se != null && se.type != 0) ? 1 : 0;
        var nw_v:Int = (nw != null && nw.type != 0) ? 1 : 0;

        var cardinalBits = w_v * 128 + s_v * 64 + e_v * 32 + n_v * 16;  // W S E N
        var cornersBits = nw_v * 8 + sw_v * 4 + se_v * 2 + ne_v * 1;  // NW SW SE NE
        var mask = cornersBits + cardinalBits;

        // if(Utils.getTilesetNo(mask) == null) {
        //     mask = cardinalBits;
        // }

        return {
            corners: cornersBits,
            cardinal: cardinalBits,
            mask: mask
        };
    }

    public static function loadMap(mapFilePath:String):Level {
        var fileContent:String = sys.io.File.getContent(mapFilePath);

        var tilesA:TilesArray = [];
        var tilesM:TilesMap = new TilesMap();

        var width:Null<Int> = null;
        var height:Null<Int> = null;

        var y:Int = 0;
        for(line in fileContent.split("\n")) {
            for(x in 0...line.length) {
                var tile:Tile = {
                    x: x,
                    y: y,
                    type: Std.parseInt(line.charAt(x))
                };
                tilesA.push(tile);
                tilesM['${x},${y}'] = tilesA.length - 1;
            }

            if(width == null) {
                width = line.length;
            }

            y++;
        }

        if(height == null) {
            height = y;
        }

        // update bitmask
        // -1,-1   0,-1    1,-1
        // -1,0    0,0     1,0
        // -1,1    0,1     1,1
        for(t in tilesA) {
            var x = t.x;
            var y = t.y;

            var selfType = (t != null && t.type != 0) ? 1 : 0;

            var n_idx:Null<Int> = tilesM['${x},${y-1}'];
            var ne_idx:Null<Int> = tilesM['${x+1},${y-1}'];
            var e_idx:Null<Int> = tilesM['${x+1},${y}'];
            var se_idx:Null<Int> = tilesM['${x+1},${y+1}'];
            var s_idx:Null<Int> = tilesM['${x},${y+1}'];
            var sw_idx:Null<Int> = tilesM['${x-1},${y+1}'];
            var w_idx:Null<Int> = tilesM['${x-1},${y}'];
            var nw_idx:Null<Int> = tilesM['${x-1},${y-1}'];

            var n:Null<Tile> = n_idx != null ? tilesA[n_idx] : null;
            var ne:Null<Tile> = ne_idx != null ? tilesA[ne_idx] : null;
            var e:Null<Tile> = e_idx != null ? tilesA[e_idx] : null;
            var se:Null<Tile> = se_idx != null ? tilesA[se_idx] : null;
            var s:Null<Tile> = s_idx != null ? tilesA[s_idx] : null;
            var sw:Null<Tile> = sw_idx != null ? tilesA[sw_idx] : null;
            var w:Null<Tile> = w_idx != null ? tilesA[w_idx] : null;
            var nw:Null<Tile> = nw_idx != null ? tilesA[nw_idx] : null;

            var bitmask = Utils.calcBitMask(n, ne, e, se, s, sw, w, nw);
            t.bitmask = bitmask;
            t.mask = bitmask.mask * selfType;  // if current tile is empty
            t.tilesetNo = Utils.getTilesetNo(t);
        }


        return {
            tilesA: tilesA,
            tilesM: tilesM,
            width: width,
            height: height
        }
    }
}
