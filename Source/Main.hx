package;

import openfl.filters.BitmapFilterShader;
import openfl.display.Tile in BaseTile;
import openfl.Assets;
import openfl.events.Event;
import openfl.display.Sprite;
import openfl.display.Tileset;
import openfl.display.Tilemap;
import openfl.geom.Rectangle;
import openfl.text.TextField;

import core.Utils;
import core.Utils.Level;


class Main extends Sprite {
	private var TS:Int;  // tile size

	private var tileset:Tileset;
	private var tilemap:Tilemap;

	private var level:Level;

	public function new () {
		super ();

		TS = 96;

		// Tiles png from:
		// http://www.angryfishstudios.com/2011/04/adventures-in-bitmasking/
		tileset = new Tileset(Assets.getBitmapData("assets/tiles.png"));
		level = Utils.loadMap('assets/level.5.map');

		updateTileset();

		stage.addEventListener(Event.RESIZE, stageOnResize);

		drawMap();
	}

	private function stageOnResize(event:Event):Void {
		drawMap();
	}

	private function drawMap():Void {
		graphics.clear();
		removeChildren();

		graphics.beginFill(0x00220a);
		graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);

		var scaleW = stage.stageWidth / level.width / TS;
		var scaleH = stage.stageHeight / level.height / TS;
		var scale = Math.min(scaleW, scaleH);

		// for(y in 0...level.height) {
        //     for(x in 0...level.width) {
		// 		graphics.beginFill(0xcecb1b);
		// 		graphics.lineStyle(1, 0xa7a7a7);
		// 		graphics.drawRect(x*TS*scale, y*TS*scale, TS*scale, TS*scale);
		// 		graphics.endFill();

		// 		var	text = new TextField();
		// 		text.x = x*TS*scale;
		// 		text.y = y*TS*scale;
		// 		var t_idx = level.tilesM['${x},${y}'];
		// 		var t = level.tilesA[t_idx];
		// 		text.text = t.mask + '\n' + t.tilesetNo + '\n' + t.bitmask.cardinal + ',' + t.bitmask.corners;
		// 		addChild(text);
		// 	}
		// }

		tilemap = new Tilemap(stage.stageWidth, stage.stageHeight, tileset);

		for(y in 0...level.height) {
            for(x in 0...level.width) {
				var t_idx = level.tilesM['${x},${y}'];
				var t = level.tilesA[t_idx];

				// if edge of map
				// if(x == 0 || x == level.width - 1 || y == 0 || y == level.height - 1) {
				// 	tilemap.addTile(new BaseTile(40, t.x*TS, t.y*TS));
				// } else if(t.tilesetNo != null) {
				// 	tilemap.addTile(new BaseTile(t.tilesetNo, t.x*TS, t.y*TS));
				// }

				if(t.tilesetNo != null) {
					tilemap.addTile(new BaseTile(t.tilesetNo, t.x*TS*scale, t.y*TS*scale, scale, scale));
				}
			}
		}

		stage.scaleX = scale;
		stage.scaleY = scale;

		addChild(tilemap);
	}

	private function updateTileset():Void {
		var TILESET_WIDTH = 8;
		var TILESET_HEIGHT = 8;
		for(y in 0...TILESET_HEIGHT) {
			for(x in 0...TILESET_WIDTH) {
				tileset.addRect(new Rectangle(x*TS, y*TS, TS, TS));
			}
		}
	}
}
